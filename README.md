# Template Injection

NodeJs script to inject an HTML (utf8 content) into a javascript script (component).

## Install

`yarn add template_injection`

## Example

`node main.js --source=src/components/**/*.js --ignore=src/**/*spec.js src/**/*tpl.js`

## Third party library

* [glob](https://www.npmjs.com/package/glob) used in the CLI to specify the source directory
* [html-minifier](https://www.npmjs.com/package/html-minifier) used to minify the template html
* [yargs](https://www.npmjs.com/package/yargs) tool to parse CLI commands


#!/usr/bin/env node
const fs = require('fs');
const glob = require('glob');
const {minify} = require('html-minifier');
const {basename, extname, resolve} = require('path');
const minifyOptions = {
  collapseBooleanAttributes:true,
  collapseInlineTagWhitespace:true,
  collapseWhitespace:true,
  customAttrCollapse: /(ng-class|ng-if|ng-repeat|uib-typeahead)/,
  minifyCSS:true,
  removeAttributeQuotes:true,
  removeComments:true
};
const args = cliArguments();
glob(args.source, {ignore: args.ignore}, injectTemplate);

/**
 * Arguments for the CLI
 */
function cliArguments () {
  return require('yargs')
    .options({
      ignore: {
        alias: 'i',
        array: true,
        describe: 'Files to ignore (use glob pattern)'
      },
      source: {
        alias: 's',
        demandOption: true,
        describe: 'The source of one or more ng components (use glob pattern)',
        type: 'string'
      },
      suffix: {
        alias: 'x',
        default: 'tpl',
        describe: 'The suffix at the end of the new file that will be create'
      }
    })
    .example('$0 --source=src/components/**/*.js --ignore=src/**/*spec.js src/**/*tpl.js')
    .argv;
}

/**
 * The callback method for the glob call.
 * Will receive an error object and the list of files found
 * @param {Error} err - Contain an error or null on success
 * @param {Array} files - List of files found
 */
function injectTemplate (err, files) {
  if (err) {
    return console.error(err.message);
  }
  for (const file of files) {
    console.info(`Processing: ${file}`);
    const filename        = basename(file);
    const extension       = extname(file);
    const filepath        = resolve(file);
    const newFilepath     = filepath.replace(extension, `.${args.suffix}${extension}`);
    deleteFile(newFilepath); // remove old file 
    const templatePath    = filepath.replace(extension, `.html`);
    const name            = filename.replace(extension, '');
    const templateName    = `${name}.html`;
    const readStream      = fs.createReadStream(filepath, {encoding: 'utf8', highWaterMark: 64*1024});
    const templateStream  = fs.createReadStream(templatePath,  {encoding: 'utf8'});
    const writeStream     = fs.createWriteStream(newFilepath);

    // IIFE to keep the scope from bein overwritten by the loop
    ((readStream, writeStream, templateStream, templateName, newFilepath) => {
      readStream.on('data', onDataMergeTemplatesToWriteStream(readStream, writeStream, templateStream, templateName));
      readStream.on('end', () => writeStream.end());
      readStream.on('error', console.error);
      writeStream.on('error', console.error);
      writeStream.on('finish', ((file) => () => console.info(`Completed: ${file}`))(newFilepath));
      templateStream.on('error', console.error);
    })(readStream, writeStream, templateStream, templateName, newFilepath);
  }; // end for
}

/**
 * Delete the old template file
 * @param {String} file - Full path
 */
function deleteFile (file) {
  try {
    fs.unlinkSync(file);
  } catch (err) {
    console.warn(err.message);
  }
}

/**
 * Two stream are read and append/merged into the write stream.
 *  
 * @param {ReadStream} readStream - The original JS file will be the first readStream
 * @param {WriteStream} writeStream - The new template file created from the merger of original JS file and template HTML
 * @param {ReadStream} templateStream - The HTLM template will be the second stream
 * @param {String} templateName - The name of the template file.
 */
function onDataMergeTemplatesToWriteStream (readStream, writeStream, templateStream, templateName) {
  return chunk => {
    let content   = chunk.toString();
    let match     = content.match(templateName);
    let index     = (match) ? match.index : 0;
    const length  = templateName.length;
    if (index) {
      readStream.pause();
      let part1 = content.substr(0, index - 1); // Subtrack single charter to remove quote ('|"")
      let part2 = content.substr(index + length + 1);
      writeStream.write(part1 + '`'); // Add the back quote '`' this will wrap the template

      templateStream.on('readable', function readable () {
        let data = this.read();
        if (data) {
          while(data) {
            writeStream.write(minify(data.toString(), minifyOptions));
            data = this.read();
          }
          writeStream.write('`' + part2);
          readStream.resume();
        }
      });

    } else {
      writeStream.write(content);
    }
  };
}
